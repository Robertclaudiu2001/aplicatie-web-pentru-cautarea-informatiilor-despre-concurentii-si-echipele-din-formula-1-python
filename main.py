from flask import Flask, render_template, request
import requests

app = Flask(__name__, template_folder='.')

first_search = True

def get_info(search_query, info_type): # Functie pentru a obtine informatii despre soferi, echipe si clasamente
    url = "https://api-formula-1.p.rapidapi.com/"
    if info_type == "drivers":
        url += f"drivers?search={search_query}"
    elif info_type == "teams":
        url += f"teams?search={search_query}"
    elif info_type == "teams_rankings":
        url += f"rankings/teams?season={search_query}"
    elif info_type == "drivers_rankings":
        url += f"rankings/drivers?season={search_query}"

    headers = {
        "X-RapidAPI-Key": "18a1b605bemsh2fa3c4b467f15a5p1ff460jsn02458571bbfe",
        "X-RapidAPI-Host": "api-formula-1.p.rapidapi.com"
    }
    response = requests.get(url, headers=headers) # Efectuarea cererii GET catre API

    if response.status_code == 200: # Verificarea daca raspunsul este OK
        data = response.json() # Obtinerea datelor sub forma de dictionar Python

        if info_type == "drivers": # Procesarea datelor in functie de tipul de informatie
            info_data = data.get('response', []) # Extrage informatiile din cheia 'response' din dicționarul data obtinut de la API
            result_info = [
                {
                    "number": item.get("number"),
                    "name": item.get("name"),
                    "image": item.get("image"),
                    "nationality": item.get("nationality"),
                    "country": item.get("country").get("name") if item.get("country") else None,
                    "birthdate": item.get("birthdate"),
                    "birthplace": item.get("birthplace"),
                    "grands_prix_entered": item.get("grands_prix_entered"),
                    "world_championships": item.get("world_championships"),
                    "podiums": item.get("podiums"),
                    "highest_grid_position": item.get("highest_grid_position"),
                    "career_points": item.get("career_points")
                }
                for item in info_data
            ]

        elif info_type == "teams":
            info_data = data.get('response', [])

            result_info = [
                {
                    "name": item.get("name"),
                    "logo": item.get("logo"),
                    "base": item.get("base"),
                    "first_team_entry": item.get("first_team_entry"),
                    "world_championships": item.get("world_championships"),
                    "pole_positions": item.get("pole_positions"),
                    "fastest_laps": item.get("fastest_laps"),
                    "president": item.get("president"),
                    "chassis": item.get("chassis"),
                    "engine": item.get("engine")
                }
                for item in info_data
            ]

        elif info_type == "teams_rankings":
            rankings_data = data.get('response', [])

            result_info = [
                {
                    "position": item.get("position"),
                    "team": {
                        "name": item.get("team", {}).get("name"),
                        "logo": item.get("team", {}).get("logo")
                    },
                    "points": item.get("points")
                }
                for item in rankings_data
            ]

        elif info_type == "drivers_rankings":
            rankings_data = data.get('response', [])

            result_info = [
                {
                    "position": item.get("position"),
                    "driver": {
                        "name": item.get("driver", {}).get("name"),
                        "image": item.get("driver", {}).get("image"),
                        "number": item.get("driver", {}).get("number")
                    },
                    "team": {
                        "name": item.get("team", {}).get("name")
                    },
                    "points": item.get("points")
                }
                for item in rankings_data
            ]

        return result_info # Returnarea datelor procesate sub forma unei liste de dictionare

@app.route('/', methods=['GET', 'POST']) # Definirea unei rute pentru pagina principala
def index():
    global first_search

    if request.method == 'POST': # Verificarea metodei cererii (GET sau POST)
        search_query = request.form.get('search_query') # Obtinerea datelor de cautare din formularul POST
        info_type = request.form.get('info_type')
        info_data = get_info(search_query, info_type) # Apelarea functiei pentru a obtine informatiile
        first_search = False

        return render_template('index.html', info_data=info_data, search_query=search_query, info_type=info_type, first_search=first_search) # Renderea template-ului HTML cu datele obtinute si variabilele necesare
    else:
        return render_template('index.html', info_data=None, search_query=None, info_type=None, first_search=first_search) # Daca metoda cererii este GET, randerarea template-ului cu variabilele initiale

if __name__ == '__main__': # Punctul de intrare in aplicatie
    app.run()

